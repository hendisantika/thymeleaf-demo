package com.hendisantika.thymeleafdemo.controller;

import org.springframework.stereotype.Controller;

/**
 * Created by IntelliJ IDEA.
 * Project : demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/08/18
 * Time: 11.51
 * To change this template use File | Settings | File Templates.
 */

@Controller
//@RequestMapping("/")
public class IndexController {
    String index() {
        return "index";
    }
}

package com.hendisantika.thymeleafdemo.controller;

import com.hendisantika.thymeleafdemo.entity.AppUser;
import com.hendisantika.thymeleafdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : siap
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 16/02/18
 * Time: 13.23
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/user")
public class AppUserController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    public AppUser createUser(@RequestBody AppUser user) {
        return userService.createUser(user);
    }

    @RequestMapping(method = RequestMethod.GET)
    public Iterable<AppUser> getAllUsers() {
        return userService.getAllUsers();
    }

    @RequestMapping(path = "{id}", method = RequestMethod.GET)
    public Iterable<AppUser> getUser(@PathVariable Long id) {
        return userService.getUser(id);
    }
}

package com.hendisantika.thymeleafdemo.repository;

import com.hendisantika.thymeleafdemo.entity.AppUser;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/08/18
 * Time: 11.46
 * To change this template use File | Settings | File Templates.
 */
public interface AppUserRepository extends CrudRepository<AppUser, Long> {

    Iterable<AppUser> findAppUserById(Long id);
}

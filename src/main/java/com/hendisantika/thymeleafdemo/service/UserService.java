package com.hendisantika.thymeleafdemo.service;

import com.hendisantika.thymeleafdemo.entity.AppUser;
import com.hendisantika.thymeleafdemo.repository.AppUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/08/18
 * Time: 11.49
 * To change this template use File | Settings | File Templates.
 */
@Service
public class UserService {

    @Autowired
    private AppUserRepository appUserRepository;

    public AppUser createUser(AppUser user) {
        return appUserRepository.save(user);
    }

    public Iterable<AppUser> getAllUsers() {

        return appUserRepository.findAll();
    }

    public Iterable<AppUser> getUser(Long id) {

        return appUserRepository.findAppUserById(id);
    }
}